import turtle as ttl

#L-System in class with method trace it
class lsystem:

    def __init__(self, axiom="", rules=[], angle=0, size=0, level=0):
        self.axiom = axiom
        self.rules = rules 
        self.angle = angle
        self.size = size
        self.level = level

        if self.angle<0 or self.angle>360 or self.size<0 or self.level<0:
            print('Error in the parameters. Could not create L-System')
            exit()

        self.currentstate = self.axiom
        if self.level != 0:
            for i in range(self.level):
                nextstage = ''
                for char in self.currentstate:
                    if char == 'a':
                        nextstage += self.rules[0]
                    elif char == 'b':
                        nextstage += self.rules[1]
                    else:
                        nextstage += char
                self.currentstate = nextstage
    
    def __call__(self, axiom, rules, angle, size, level):
        return self.__init__(axiom, rules, angle, size, level)

    def changeLevel(self, n):
        self.level = n
        if self.level<0:
            print('Level cannot be negative')
            exit()
        self.currentstate = self.axiom
        if self.level != 0:
            for i in range(self.level):
                nextstage = ''
                for char in self.currentstate:
                    if char == 'a':
                        nextstage += self.rules[0]
                    elif char == 'b':
                        nextstage += self.rules[1]
                    else:
                        nextstage += char
                self.currentstate = nextstage

    def show(self):
        lastpos = ()
        for char in self.currentstate:
            if char == 'a':
                ttl.pd(); ttl.fd(self.size)
            elif char == 'b':
                ttl.pu(); ttl.fd(self.size)
            elif char == '+':
                ttl.right(self.angle)
            elif char == '-':
                ttl.left(self.angle)
            elif char == '*':
                ttl.right(180)
            elif char == '[':
                lastpos = (ttl.heading(), ttl.position())
            elif char == ']':
                ttl.setheading(lastpos[0]); ttl.setposition(lastpos[1][0],lastpos[1][1])
            else:
                print('Invalid character in axiom or in rule. Check then try again.')
                break
                exit()
        ttl.exitonclick()
    
    def toFile(self,filename):
        out_file = open(filename, 'a+')
        out_file.truncate(0)
        for line in range(7):
            if line==0:
                output = 'axiom = "{}"\n'.format(self.axiom)
            elif line==1:
                output = 'rules = \n'
            elif line==2:
                output = 'a="{}"\n'.format(self.rules[0])
            elif line==3:
                output = 'b="{}"\n'.format(self.rules[1])
            elif line==4:
                output = 'angle = {}\n'.format(str(self.angle))
            elif line==5:
                output = 'size = {}\n'.format(str(self.size))
            elif line==6:
                output = 'level = {}'.format(str(self.level))
            out_file.write(output)
        out_file.close()



        

#Read L-System info from file    
def readfile(filepath):
    input_file = open(filepath, 'r')
    lines = input_file.readlines()
    input_file.close()
    
    for i in range(len(lines)):
        lines[i] = lines[i].strip()

    start = 0
    cnt = 0
    for char in lines[0]:
        if char == '"':
            start = cnt
            break 
        cnt += 1
    axiom = lines[0][start+1:len(lines[0])-1]
    
    rules= []
    for i in range(2,4):
        rules.append(lines[i][3:len(lines[i])-1])

    angle = int(lines[4][8:len(lines[4])])

    size = int(lines[5][7:len(lines[5])])

    level = int(lines[6][8:len(lines[6])])
    return lsystem(axiom, rules, angle, size, level)