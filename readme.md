# PROJECT ON L-SYSTEMS YASH ORI, GABRIEL ALBINO

## Observations 
The program was written on Python 3.7.4 and has not been tested on other versions. <br>
Check your Python version with `$python3 --version` (Linux) or `>python --version` (Windows)<br> 
Install <a href="http://www.python.org">Python</a>

## Requirements
Running this program does not require any additional libraries.

## How does it work?
In the main.py file:<br>
<ul>
    <li> You will notice <code>import LSystem as lsys</code> this line imports the "LSystem" file contains the L-System object and the necessary methods associated to it. <br>
    <li>You can create an L-System object either by inputting all the parameters manually using the syntax <code>system = lsys.lsystem(axiom, rules, angle, size, level)</code> <br>
    With the following data types:  <br>
    <ul>
        <li>axiom: string</li>
        <li>rules: list of strings</li>
        <li>angle: int>0</li>
        <li>size: int>0</li>
        <li>level: int>0</li>
    </ul>
    <br>
    <li>Alternatively you can read the data from a file with the following syntax <code>system = lsys.readfile('path_to_file')</code><br>
    The file should be in the following format: <br>
    <pre>
    axiom = "xxxxxxx"
    rules = 
    "a=xxxxxxxx"
    "b=xxxxxxxx"
    angle = xx
    size = xx
    level = xx</pre>
    <li>For the L-System alphabet used for the axiom and the rules it is as follows:
    <ul>
        <li>"a" : <code>turtle.pd(); turtle.fd(size)</code> (Move and trace a line)
        <li>"b" : <code>turtle.pu(); turtle.fd(size)</code> (Move without tracing a line)
        <li>"+" : <code>turtle.right(angle)</code> (Turn right at the given angle)
        <li>"-" : <code>turtle.left(angle)</code> (Turn left at the given angle)
        <li>"*" : <code>turtle.right(180)</code> (Rotate 180°)
        <li>"[" : <code>lastpos = (turtle.heading(), turtle.position())</code> (Memorise current position)
        <li>"]" : <code>turtle.setheading(lastpos[0]); turtle.setposition(lastpos[1][0],lastpos[1][1])</code> (Go to last remembered position)
    </ul>
    <li>Finally to show the L-System just call the show method <code>system.show()</code>
    <li>If you would like to save a L-System as a file to read it afterwards you can do so with te following syntax <code>system.toFile("filename")</code>
    <li> Finally to run the program:
    <ul>
        <li> Open Terminal/Command Prompt
        <li> <code>cd directoryOfProgram</code>
        <li> <code>$python3 main.py</code> (Linux)
        <li> <code>>python main.py</code> (Windows)
    </ul>
</ul>

## Limitations
We were not able to implement the change of level using the common `system.level = n` syntax. If you use it it will not update the string that handles the drawing. This shortcoming is fixed with the implementation of a method that updates everything at once. It can be called with the syntax `system.changeLevel(n)` where n is a positive integer representing the new level.
